
// include modules
var gulp      = require('gulp');
var sass      = require('gulp-sass');
var plumber   = require('gulp-plumber');
var watch     = require('gulp-watch');
var imagemin  = require('gulp-imagemin');
var minifyCSS = require('gulp-minify-css');
var uglify    = require('gulp-uglify');
var del       = require('del');
var include   = require('gulp-include');
var rename    = require('gulp-rename');

// defines paths
var paths = {
  sources: 'www/src',
  build:   'www/build'
};

// We wil watch all SCSS files but only generate the
// top level ones as output
paths.css        = {};
paths.css.input  = [paths.sources + '/css/all.scss',paths.sources + '/css/vendor.scss',paths.sources + '/css/app.scss'];
paths.css.watch  = [paths.sources + '/css/**/*.scss'];
paths.css.build  = paths.build + '/css/';

// We wil watch all JS files but only generate the
// top level ones as output
paths.js          = {};
paths.js.input    = [paths.sources + '/js/all.js',paths.sources + '/js/vendor.js',paths.sources + '/js/app.js'];
paths.js.watch    = [paths.sources + '/js/**/*.js'];
paths.js.build    = paths.build + '/js/';

// Image input and watch are the same
paths.img         = {};
paths.img.input   = [paths.sources + '/img/**/*'];
paths.img.watch   = paths.img.input;
paths.img.build   = paths.build + '/img/';

// Font input and watch are the same
paths.fonts         = {};
paths.fonts.input   = [paths.sources + '/fonts/**/*'];
paths.fonts.watch   = paths.fonts.input;
paths.fonts.build   = paths.build + '/fonts/';

gulp.task('clean', function(cb) {
  // You can use multiple globbing patterns as you would with `gulp.src`
  del([paths.build], cb);
});

// Default task is to generate distributions
gulp.task('default', ['fonts','images','styles','scripts'], function () {

});

// Generates the distribution files
gulp.task('dist', ['fonts','images','min-css','min-js'], function () {


});

// Minifies the created CSS files
gulp.task('min-css', ['styles'], function() {
    // We have to exclude the already minified files (.min suffix)
    return gulp.src([paths.css.build + '*.css', '!' + paths.css.build + '*.min.css'])
        .pipe(minifyCSS({keepBreaks:false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.css.build));    
});

// Minifies the creates JS files
gulp.task('min-js', ['scripts'], function() {
    // We have to exclude the already minified files (.min suffix)
    return gulp.src([paths.js.build + '*.js', '!' + paths.js.build + '*.min.js'])
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.js.build));   
});

// Generate CSS
gulp.task('styles', function () {
    var onError = function(err) {
        console.log(err.toString());
        this.emit('end')
    };
    return gulp.src(paths.css.input)
            .pipe(plumber({ errorHandler: onError }))
            .pipe(sass())
            .pipe(gulp.dest(paths.css.build));
});

// Generate scripts
gulp.task('scripts',function () {
    // Simply copy javascript files over
    return gulp.src(paths.js.input)
        .pipe( include() )
        .pipe(gulp.dest(paths.js.build));
});

// Generate images
gulp.task('images', function() {
    // copy images over and optimize them for size
    return gulp.src(paths.img.input)
        // pass in options to the task
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest(paths.img.build));
});

// Copies the fonts
gulp.task('fonts', function() {
    // copy fonts over
    return gulp.src(paths.fonts.input)
        .pipe(gulp.dest(paths.fonts.build));
});

// Use this for development
gulp.task('watch', function () {
    watch(paths.css.watch, ['styles'])
    watch(paths.js.watch,['scripts'])
    watch(paths.img.watch,['images'])
    watch(paths.fonts.watch,['fonts'])
});
