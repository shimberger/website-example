<?php

// Define constants for environment
define('ENV_PRODUCTION','prod');
define('ENV_DEVELOPMENT','dev');

// include the env.php file if it exists
@include(dirname(__FILE__).'/env.php');

if (!defined('ENVIRONMENT')) {
	// Set the environment to production if not set
	define('ENVIRONMENT',ENV_PRODUCTION);
}

// function to check if we are in the production environment
function is_prod_env() 
{
	return ENVIRONMENT == ENV_PRODUCTION;
}

?>