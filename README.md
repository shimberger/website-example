Website
=======

This an example for a gulp powered website.
Clone repository to get started.

Getting started with development
--------------------------------
**Setting up the software**

Make sure you have the following software installed:

* node.js
* npm

Test this by running the following commands. First run the following 
command to test if node is installed:

	node -v

Run the following command to test for npm:

	npm -v

Afterwards install gulp as a global module.

	sudo npm install -g gulp

Make sure you can execute gulp. By typing:

	gulp -v

If you can not execute gulp your PATH variable is not set up correctly.

Now install bower by installing it as a global module.

	sudo npm install -g bower

Make sure it works by typing:

	bower -v 

Awesome! Now install all the modules required by gulp by just typing:

	npm install

This will create the node_modules folder.

Install all the frontend dependencies by typing:

	bower install

This will most likely do nothing because the dependencies are checked into git.

**Using gulp & directory structure**

Now we are ready to use gulp. The gulpfile configures the tasks. Take a look inside if you want.

To understand how it works lets fist look at the directory structure. The website is under
the ``www`` folder. There have the following three top folders:

* ``bower_components`` holds the dependencies installed via bower
* ``src`` holds the source code for the frontend (css, js, img)
* ``build`` holds the generated frontend code. 
 **Important**: You should never modify files in this directory.

Lets dive into the ``src`` directory. Here we have the directories: ``css``, ``img`` and ``js``. 

The ``img`` directory is pretty simple. It contains the images for your site. Organize it however you want. The structure will just be copied over into ``build/img``. During the copying images will be optimized but nothing more will be done.

The ``css`` and ``js`` directories are similar in structure. They hold three files: ``app``, ``vendor`` and ``all``. 

* The ``app`` file should hold all the code you write for your site. 
* The ``vendor`` file is used to include source code installed via bower. 
* The ``all`` file includes the ``vendor`` and ``app`` file. 

This is useful so you can just use the ``all`` file during production but while developing you have your 3rd party code separate from your own. 

**Only these three files (app,vendor,all) will be processed by gulp**. So you can structure the rest however you like. Just make sure your code is included into the ``app`` file (using either sass imports or gulp-includes).

The most important gulp tasks are:

* *default*: Generates the development resources (images, styles, javascript)
* *dist*: Generates the resources & minifies them
* *watch*: Starts gulp in watch mode so it automatically regenerates resources when you change the source

Run these tasks by executing:

	gulp <task> // e.g gulp dist
